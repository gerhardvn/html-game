function letterFrequency(text) {
  // Initialize an empty object to store the frequency count
  const frequency = {};
  const digraphs = {};
  const trigrams = {};
  
  // Loop through each character in the text
  for (let i = 0; i < text.length; i++) {
    // Ignore non-alphabetic characters
    if (!/[a-zA-Z]/.test(text[i])) {
      continue;
    }

    // Convert the character to lowercase to avoid counting the same letter twice
    const letter = text[i].toLowerCase();

    // If the letter hasn't been counted yet, initialize its count to 0
    if (!frequency[letter]) {
      frequency[letter] = 0;
    }

    // Increment the letter count
    frequency[letter]++;
    
    if (!/[a-zA-Z]/.test(text[i+1])) {
      continue;
    }
    const digraph = text.substr(i, 2);
    if (!digraphs[digraph]) {
      digraphs[digraph] = 1;
    } else {
      digraphs[digraph]++;
    }

    if (!/[a-zA-Z]/.test(text[i+2])) {
      continue;
    }
    const trigram = text.substr(i, 3);
    if (!trigrams[trigram]) {
      trigrams[trigram] = 1;
    } else {
      trigrams[trigram]++;
    }
  }
  
  // Sort the digraph data by count in descending order
  const digraphsSorted = [];
  for (const digraph in digraphs) {
    digraphsSorted.push({ label: digraph, count: digraphs[digraph] });
  }
  digraphsSorted.sort((a, b) => b.count - a.count);

  // Sort the trigram data by count in descending order
  const trigramsSorted = [];
  for (const trigram in trigrams) {
    trigramsSorted.push({ label: trigram, count: trigrams[trigram] });
  }
  trigramsSorted.sort((a, b) => b.count - a.count);
  
  return {
    frequency: frequency,
    digraphs: digraphsSorted.slice(0, 10),
    trigrams: trigramsSorted.slice(0, 5),
  };
}

function analyze() {
  // Get the input text from the textarea
  const input = document.getElementById("input-text").value;

  // Analyze the letter frequency
  const { frequency, digraphs, trigrams } = letterFrequency(input);

  // Convert the frequency object to arrays for use with Chart.js
  let labels = Object.keys(frequency);
  let data = Object.values(frequency);

  // Get the selected sorting order
  const sortOrder = document.getElementById("sort-order").value;

  // Sort the frequency data based on the selected sorting order
  if (sortOrder === "alphabetical") {
    labels.sort();
    data = labels.map(letter => frequency[letter]);
  } else if (sortOrder === "descending") {
    const frequencySorted = [];
    for (let i = 0; i < labels.length; i++) {
      frequencySorted.push({ label: labels[i], count: data[i] });
    }
    frequencySorted.sort((a, b) => b.count - a.count);
    labels = frequencySorted.map(item => item.label);
    data = frequencySorted.map(item => item.count);
  }

  // Create a bar chart using Chart.js
  const ctx = document.getElementById("chart").getContext("2d");
  const chart = new Chart(ctx, {
    type: "bar",
    data: {
      labels: labels,
      datasets: [{
        label: "Letter Frequency",
        data: data,
        backgroundColor: "rgba(54, 162, 235, 0.2)",
        borderColor: "rgba(54, 162, 235, 1)",
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });

  // Format the frequency count as a string with percentages
  let result = "";
  const total = input.length;
  if (sortOrder === "alphabetical") {
    for (const letter of labels) {
      const count = frequency[letter];
      const percent = ((count / total) * 100).toFixed(2);
      result += `${letter}: ${count} (${percent}%)\n`;
    }
  } else if (sortOrder === "descending") {
    for (const letter in frequency) {
      const count = frequency[letter];
      const percent = ((count / total) * 100).toFixed(2);
      result += `${letter}: ${count} (${percent}%)\n`;
    }
  }

  result += "\n\nDigraphs\n";
  for (const digraph of digraphs) {
    const count = digraph.count;
    const percent = ((count / total) * 100).toFixed(2);
    result += `${digraph.label}: ${count} (${percent}%)\n`;
  }

  result += "\nTrigrams\n";
  for (const trigram of trigrams) {
    const count = trigram.count;
    const percent = ((count / total) * 100).toFixed(2);
    result += `${trigram.label}: ${count} (${percent}%)\n`;
  }

  // Display the frequency count in the HTML result element
  document.getElementById("result").textContent = result;
}
