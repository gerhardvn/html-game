var myObstacles = [];
var myGamePiece;
var myScore;

function startGame() {
  myGameArea.start();
  myGamePiece = new component(30, 30, "red", 10, 120);
  myScore = new component("30px", "Consolas", "black", 280, 40, "text"); 
}

var myGameArea = {
  canvas : document.createElement("canvas"),
  
  start : function() {
    this.canvas.width = 480;
    this.canvas.height = 270;
    this.context = this.canvas.getContext("2d");
    document.body.insertBefore(this.canvas, document.body.childNodes[0]);
    
    this.frameNo = 0;  
    this.interval = setInterval(updateGameArea, 20);
    
    // Keys
    window.addEventListener('keydown', function (e) {
      myGameArea.keys = (myGameArea.keys || []);
      myGameArea.keys[e.keyCode] = true;
    })
    window.addEventListener('keyup', function (e) {
      myGameArea.keys[e.keyCode] = false;
    })
    
  },
  
  clear : function() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  },
  
  stop : function() {
    clearInterval(this.interval);
  }
}

function everyinterval(n) {
  if ((myGameArea.frameNo / n) % 1 == 0) {return true;}
  return false;
}

function component(width, height, color, x, y, type) {
 
  this.type = type;
  
  this.width = width;
  this.height = height;
  this.angle = 0;
  this.speed = 1;
  this.moveAngle = 1;
  this.x = x;
  this.y = y;
  
  // Draw
  this.update = function(){
    ctx = myGameArea.context;
    
    if (this.type == "text") {
      ctx.font = this.width + " " + this.height;
      ctx.fillStyle = color;
      ctx.fillText(this.text, this.x, this.y);
    } else {
      ctx = myGameArea.context;
      ctx.save();
      ctx.translate(this.x, this.y);
      ctx.rotate(this.angle);
      ctx.fillStyle = color;
      ctx.fillRect(this.width / -2, this.height / -2, this.width, this.height);
      ctx.restore();
    }
  }
  
  this.newPos = function() {
    this.angle += this.moveAngle * Math.PI / 180;
    this.x += this.speed * Math.sin(this.angle);
    this.y -= this.speed * Math.cos(this.angle);
  }
 
  this.crashWith = function(otherobj) {
    var myleft = this.x;
    var myright = this.x + (this.width);
    var mytop = this.y;
    var mybottom = this.y + (this.height);
    var otherleft = otherobj.x;
    var otherright = otherobj.x + (otherobj.width);
    var othertop = otherobj.y;
    var otherbottom = otherobj.y + (otherobj.height);
    var crash = true;
    if ((mybottom < othertop) ||
    (mytop > otherbottom) ||
    (myright < otherleft) ||
    (myleft > otherright)) {
      crash = false;
    }
    return crash;
  }
}

function updateGameArea() {
  
  var x, height, gap, minHeight, maxHeight, minGap, maxGap;
  
  for (i = 0; i < myObstacles.length; i += 1) {
    if (myGamePiece.crashWith(myObstacles[i])) {
      myGameArea.stop();
      return;
    }
  }
  
  myGameArea.clear();
  myGameArea.frameNo += 1;
  
  if (myGameArea.frameNo == 1 || everyinterval(150)) {
    x = myGameArea.canvas.width;
    minHeight = 20;
    maxHeight = 200;
    height = Math.floor(Math.random()*(maxHeight-minHeight+1)+minHeight);
    minGap = 50;
    maxGap = 200;
    gap = Math.floor(Math.random()*(maxGap-minGap+1)+minGap);
    myObstacles.push(new component(10, height, "green", x, 0));
    myObstacles.push(new component(10, x - height - gap, "green", x, height + gap));
  }
  
  for (i = 0; i < myObstacles.length; i += 1) {
    myObstacles[i].x += -1;
    myObstacles[i].update();
  }
  
  // translate key press to movement
  if (myGameArea.keys){
    myGamePiece.speed = 0;
    myGamePiece.moveAngle = 0;
    if (myGameArea.keys[37]) {myGamePiece.moveAngle = -1; }
    if (myGameArea.keys[39]) {myGamePiece.moveAngle = +1; }
    if (myGameArea.keys[38]) {myGamePiece.speed = -1; }
    if (myGameArea.keys[40]) {myGamePiece.speed = +1; }
  }
   
  myScore.text = "SCORE: " + myGameArea.frameNo;
  myScore.update();

  myGamePiece.newPos();
  myGamePiece.update();
}

function stopMove() {
  myGamePiece.speed = 0;
}


startGame();